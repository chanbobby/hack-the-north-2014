package angryunicorn.freerider;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mapbox.mapboxsdk.overlay.GpsLocationProvider;
import com.mapbox.mapboxsdk.overlay.UserLocationOverlay;
import com.mapbox.mapboxsdk.tileprovider.tilesource.MapboxTileLayer;
import com.mapbox.mapboxsdk.views.MapView;

import org.json.simple.parser.JSONParser;
import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;

import ai.wit.sdk.IWitListener;
import ai.wit.sdk.Wit;

public class MainActivity extends ActionBarActivity implements IWitListener {

    LocationManager locMan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        /* Yelp API Test Code */
//        YelpAPI yapi = new YelpAPI();
//        String res = yapi.searchForBusinessesByLocation("coffee", "Waterloo, Ontario");
//        System.out.println("YELP: "+res);


//        setContentView(R.layout.activity_main);
//        this.findViewById(R.id.mapview);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        //setupMapbox();


        locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    }

    public void setupMapbox() {
        XmlPullParser parser = getResources().getXml(R.xml.attrs);
        AttributeSet attrs = Xml.asAttributeSet(parser);
        MapView mapView = new MapView(this, attrs);
        mapView.setTileSource(new MapboxTileLayer("backpackerhtn.jc18ilgb"));
//        setContentView(mapView);

        GpsLocationProvider gps = new GpsLocationProvider(this);
        UserLocationOverlay myLocationOverlay = new UserLocationOverlay(gps, mapView);
        myLocationOverlay.enableMyLocation();
        myLocationOverlay.enableFollowLocation();
        myLocationOverlay.setDrawAccuracyEnabled(true);
        mapView.getOverlays().add(myLocationOverlay);
        mapView.setCenter(myLocationOverlay.getMyLocation());

        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void witDidGraspIntent(String intent, HashMap<String, JsonElement> entities, String body, double confidence, Error error) {
//        ((TextView) findViewById(R.id.txtText)).setText(body);
//        Gson gson = new GsonBuilder().setPrettyPrinting().create();
//        String jsonOutput = gson.toJson(entities);
//        ((TextView) findViewById(R.id.jsonView)).setText(Html.fromHtml("<span><b>Intent: " + intent +
//                "<b></span><br/>") + jsonOutput +
//                Html.fromHtml("<br/><span><b>Confidence: " + confidence + "<b></span>"));


        if(intent.equalsIgnoreCase("find_location") && entities.containsKey("local_search_query")){
            JsonElement lsq = entities.get("local_search_query");

            if(lsq.isJsonObject()){
                String value = ((JsonObject)lsq).getAsJsonPrimitive("value").getAsString();
                YelpAPI yapi = new YelpAPI();
                Location loc = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                String result  = yapi.searchForBusinessesByLocation(value, loc);
//                ((TextView) findViewById(R.id.txtText)).setText(result);


                System.out.println("RESULT: "+result);


                JSONParser parser = new JSONParser();
                org.json.simple.JSONObject resObj = null;
                org.json.simple.JSONObject coord = null;
                Double lat, lon = 0.0;
                try{
                    resObj = (org.json.simple.JSONObject) parser.parse(result);
                    org.json.simple.JSONArray busns = (org.json.simple.JSONArray)resObj.get("businesses");
                    coord = (org.json.simple.JSONObject)((org.json.simple.JSONObject)((org.json.simple.JSONObject)busns.get(0)).get("location")).get("coordinate");

                    if(coord.equals(null)){
                        System.out.println("EXCEPTION: NULL target coordinates!");
                        return;
                    }

                    lat = Double.valueOf((Double)coord.get("latitude"));
                    lon = Double.valueOf((Double)coord.get("longitude"));
                }
                catch (Exception e){
                    System.out.println("EXCEPTION: "+e.toString());
                    return;
                }







                routeThisForMe(new LatLong(loc), new LatLong(lat, lon));


            }
            else{
                System.out.println("Error FRAK YOU 2.1");
            }
        }
        else{
            System.out.println("Error FRAK YOU 1");
        }


    }



    public void routeThisForMe(LatLong from, LatLong to){
        //TODO!!!
        //Bobby, Do your magical shit here

        System.out.println("ROUTING from: "+from+", to: "+to);
        
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            // Initialize Fragment
            Wit wit_fragment = (Wit) getFragmentManager().findFragmentByTag("wit_fragment");
            if (wit_fragment != null) {
                wit_fragment.setAccessToken("7IKJ3YGYONUCZPTACC7CWUHDHCKXLWU6");
            }

            return rootView;
        }
    }
}


class LatLong {
    public Double latitude;
    public Double longitude;

    public LatLong(Double lat, Double lon){
        this.latitude = lat;
        this.longitude = lon;
    }

    public LatLong(Location loc){
        this.latitude = loc.getLatitude();
        this.longitude = loc.getLongitude();
    }

    public String toString(){
        return "LatLong:("+String.valueOf(this.latitude)+", "+String.valueOf(this.longitude)+")";
    }
}
